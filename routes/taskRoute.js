const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

// route to get a specific task
router.get("/:id", (req, res) => {
    taskController
        .getSpecificTask(req.params.id)
        .then((resultFromController) => res.send(resultFromController));
});

// route to update status of a specific task
router.put("/:id/:status", (req, res) => {
    taskController
        .updateTaskStatus(req.params.id, req.params.status)
        .then((resultFromController) => res.send(resultFromController));
});

// route to get all the task
router.get("/", (req, res) => {
    taskController
        .getAllTasks()
        .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
