const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");
const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/tasks", taskRoute)

mongoose.connect(
    "mongodb+srv://admin:admin@zuittbatch243.fvxpkh7.mongodb.net/B243-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});