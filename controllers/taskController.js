const Task = require("../models/task.js");

// for getting a specific task using id
module.exports.getSpecificTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return result;
        }
    });
};

// for updating status of a specific task using id
module.exports.updateTaskStatus = (taskId, newStatus) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        }

        result.status = newStatus;
        return result.save().then((updatedTaskStatus, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return updatedTaskStatus;
            }
        });
    });
};

// for getting all task to see the all the ids available
module.exports.getAllTasks = () => {
    return Task.find({}).then((result) => {
        return result;
    });
};